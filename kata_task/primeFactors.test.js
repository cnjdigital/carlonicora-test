
/* primeFactors.test.js */
jQuery(function($) {
  var outputEl = $('#output');
  
  function testPrimeFactors(primeFactors) {
    for(var i in primeFactors) {
      console.log(getPrimeFactors(primeFactors[i]))
      outputEl
        .append(getPrimeFactors(primeFactors[i]).join(','))
        .append('<br>')
    }
  }
  
  testPrimeFactors([1,4,6,8,9,100,147])
})