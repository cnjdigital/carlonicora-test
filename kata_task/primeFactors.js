function getPrimeFactors(number) {
  var result = [];
  var divider = 2;

  while(number > 1) {
    if(number % divider != 0) {
      divider++;        
      continue;
    }

    result.push(divider);
    number = number / divider;
  }

  return result;
}